package DTUpay;

import org.junit.Assert;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import dtu.ws.fastmoney.*;
import java.math.BigDecimal;
import java.util.ArrayList;

public class ExampleSteps {
	
	DTUpay dtuPay;
	Bank bank;
	Token token;
	
	DTUpayUser dtuPayCustomer = new DTUpayUser();
	DTUpayUser dtuPayMerchant = new DTUpayUser();
	
	ArrayList<Token> tokenArrayList = new ArrayList<Token>();
	BigDecimal amount = new BigDecimal(100);	
	BigDecimal initialBalance = new BigDecimal(10000);
	
	User customer = new User();
	User merchant = new User();
	
	String customerCPR = "123456";
	String merchantCPR = "654321";
	
	String customerBankAccountID;
	String merchantBankAccountID;
	
	boolean succeeds;
	
	public ExampleSteps() {
		bank   = new Bank();
		dtuPay = new DTUpay(bank);
	}
	
	@Given("customer has bank account")
	public void customerHasBankAccount() throws BankServiceException {
	    customer.setCprNumber("123456");
	    customer.setFirstName("lee");
	    customer.setLastName("wang");
		customerBankAccountID = bank.createAccountWithBalance(customer, initialBalance);
		
	}

	@Given("customer has account in DTUpay")
	public void customerHasAccountInDTUpay() {
	    dtuPay.createAccount(customerCPR, dtuPayCustomer.getName());
	}

	@Given("customer has at least one unused token")
	public void customerHasAtLeastOneUnusedToken() {
		if (tokenArrayList.size() <= 1) {
		    tokenArrayList = dtuPay.requestTokens();
		}
	}

	@Given("merchant has bank account")
	public void merchantHasBankAccount() throws BankServiceException {
	    merchant.setCprNumber("654321");
	    merchant.setFirstName("wang");
	    merchant.setLastName("lee");
		merchantBankAccountID = bank.createAccountWithBalance(merchant, initialBalance);
	}

	@Given("merchant has account in DTUpay")
	public void merchantHasAccountInDTUpay() {
		dtuPay.createAccount(merchantCPR, dtuPayMerchant.getName());
	}

	@When("merchant scans the token")
	public void merchantScansTheToken() throws BankServiceException {
	    token = tokenArrayList.get(0);
	    dtuPay.bank = bank;
	    succeeds = dtuPay.transfer(token, customerBankAccountID, merchantBankAccountID, amount);
	}

	@Then("the payment succeeds")
	public void thePaymentSucceeds() {
	    Assert.assertTrue(succeeds);
	}

	@Then("money transfered from the customer account to merchant account in the bank")
	public void moneyTransferedFromTheCustomerAccountToMerchantAccountInTheBank() throws BankServiceException {
	    Assert.assertEquals((initialBalance.subtract(amount)), bank.getAccountByCprNumber(customerCPR).getBalance());
	}
	
	@Given("customer has one used token")
	public void customerHasOneUsedToken() throws BankServiceException {

		if (tokenArrayList.size() <= 1) {
		    tokenArrayList = dtuPay.requestTokens();
		}
	    token = tokenArrayList.get(0);
	    dtuPay.transfer(token, customerBankAccountID, merchantBankAccountID, amount);
	}

	@Then("the payment fails")
	public void thePaymentFails() {
		Assert.assertFalse(succeeds);
	}

	@Then("customer and merchant have the same amount of money as before")
	public void customerAndMerchantHaveTheSameAmountOfMoneyAsBefore() throws BankServiceException {
		Assert.assertEquals((initialBalance), bank.getAccountByCprNumber(customerCPR).getBalance());
	}
	
	@Given("customer has one fake token")
	public void customerHasOneFakeToken() {
	    Token fakeToken = new Token();
	    tokenArrayList.clear();
	    tokenArrayList.add(fakeToken);
	}
	
}

