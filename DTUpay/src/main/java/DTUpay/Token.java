package DTUpay;

import java.util.UUID;

public class Token {
    
    private String tokenString;
    private boolean used;
    
    public Token() {
        this.tokenString = UUID.randomUUID().toString();
    	this.used = false;
    }
    
    public boolean getTokenUsed() {
        return used;
    }
    
    public void setTokenUsed() {
        this.used = true;
    }
    
    public String getTokenId() {
        return tokenString;
    }

}
