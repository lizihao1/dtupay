package DTUpay;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import dtu.ws.fastmoney.Bank;
import dtu.ws.fastmoney.BankServiceException;

public class DTUpay {
	
	public DTUpay(Bank bank) {
		this.bank = bank;
	}
	
	HashMap<String,String> acctInfo = new HashMap<String,String>();
	HashMap<String,ArrayList<Token>> acctToken = new HashMap<String,ArrayList<Token>>();
	ArrayList<Token> tokenArrayList = new ArrayList<Token>();
	Bank bank = new Bank();
	
	public void createAccount(String customerCPR, String name) {
		acctInfo.put(customerCPR, name);
		Token token = new Token();
	    tokenArrayList.add(token);
		acctToken.put(customerCPR,tokenArrayList);
	}

	public ArrayList<Token> requestTokens() {
	    Token token = new Token();
	    tokenArrayList.add(token);
	    
		return tokenArrayList;
	}
	
	public ArrayList<Token> addUsedToken() {
		Token token = new Token();
		token.setTokenUsed();
		tokenArrayList.clear();
		tokenArrayList.add(token);
		
		return tokenArrayList;
	}

	public boolean transfer(Token token,String customerId, String merchantId, BigDecimal amount) throws BankServiceException {
		if (!token.getTokenUsed() && tokenArrayList.contains(token)) {
    		bank.transferMoneyFromTo(customerId, merchantId, amount, "DTUPay Transaction");
    		tokenArrayList.get(0).setTokenUsed();
    		
    		return true;
		}
		
		return false;
	}
	
	
}
